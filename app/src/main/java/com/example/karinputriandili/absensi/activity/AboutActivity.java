package com.example.karinputriandili.absensi.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.karinputriandili.absensi.R;

public class AboutActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    TextView judul, about, about2, about3, about4, about5, about6,back, menu;

    String a1, a2, a3, a4, a5, a6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setFitsSystemWindows(true);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        judul = (TextView) findViewById(R.id.txtJudul);
        about = (TextView) findViewById(R.id.txtAbout);
        about2 = (TextView) findViewById(R.id.txtAbout2);
        about3 = (TextView) findViewById(R.id.txtAbout3);
        about4 = (TextView) findViewById(R.id.txtAbout4);
        about5 = (TextView) findViewById(R.id.txtAbout5);
        about6 = (TextView) findViewById(R.id.txtAbout6);

        a1 = "SMAN 4 MEDAN" +
                " \n \nBertempat di Jalan Gelas No.12 Medan ";
        a2 = "Phone: (061) 4158244";
        a3 = "Sekolah ini didirikan pada tanggal 10 September 1961";
        a4 = "Kepala Sekolah saat ini adalah Bapak Drs.Ramli";
        a5 = "Termasuk SMA populer di Kota Medan";
        a6 = "Email : smanegeri4medan@yahoo.com";

        about.setText(String.valueOf(a1));
        about2.setText(String.valueOf(a2));
        about3.setText(String.valueOf(a3));
        about4.setText(String.valueOf(a4));
        about5.setText(String.valueOf(a5));
        about6.setText(String.valueOf(a6));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
