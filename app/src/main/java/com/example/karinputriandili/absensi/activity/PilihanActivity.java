package com.example.karinputriandili.absensi.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.karinputriandili.absensi.Helper;
import com.example.karinputriandili.absensi.R;
import com.example.karinputriandili.absensi.api.ApiClient;
import com.example.karinputriandili.absensi.api.entity.RoomEntity;
import com.example.karinputriandili.absensi.api.entity.SchoolEntity;
import com.example.karinputriandili.absensi.api.entity.StudentEntity;
import com.example.karinputriandili.absensi.api.model.ApiResponse;
import com.example.karinputriandili.absensi.api.model.RoomResponse;
import com.example.karinputriandili.absensi.api.model.SchoolsResponse;
import com.example.karinputriandili.absensi.ui.DatePickerFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PilihanActivity extends AppCompatActivity {

    protected Toolbar toolbar;

    private GoogleMap Gmap;
    private LocationManager locationManager;
    private Location location;
    Circle circle;

    private TextView txt_welcome, txt_date;
    private MaterialSpinner spinnerRoom, spinnerStatus;
    private Calendar selectedDate;
    private FrameLayout frm_date;
    private Button btn_simpan;

    private StudentEntity student;
    private SchoolEntity school;
    private List<RoomEntity> listRoom;

    private Boolean checkRadius;

    String[] daftarStatus = {
            "Hadir",
            "Sakit",
            "Izin",
            "Absen"
    };

    private OnMapReadyCallback mapListener = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap map) {
            Gmap = map;

            setSchoolLocation();
            Criteria criteria = new Criteria();
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            locationManager.requestLocationUpdates(bestProvider, 1000, 20, locationListener);
            location = locationManager.getLastKnownLocation(bestProvider);
            addLocationMarker(location);
        }
    };

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location _location) {
            // TODO Auto-generated method stub
            location = _location;
            addLocationMarker(location);
            if (Build.VERSION.SDK_INT >= 23 &&
                    ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(this);
            Log.d("debugku", location.getLatitude() + "-" + location.getLongitude());
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilihan);

        Intent intent = getIntent();
        student = (StudentEntity) intent.getSerializableExtra("user");

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setFitsSystemWindows(true);
        setSupportActionBar(toolbar);

        initView();
        setEvent();

        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view);
        map.getMapAsync(mapListener);
        map.getView().setClickable(false);

        // Here, thisActivity is the current activity
        if(Build.VERSION.SDK_INT>=23){
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        getRooms();
    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(getApplicationContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                }
            });
            dialog.show();
        }
    }

    private void initView(){
        spinnerRoom = (MaterialSpinner)findViewById(R.id.spinner_room);
        spinnerStatus = (MaterialSpinner)findViewById(R.id.spinner_status);
        txt_welcome = (TextView)findViewById(R.id.txt_welcome);
        txt_date = (TextView)findViewById(R.id.txt_date);
        frm_date = (FrameLayout)findViewById(R.id.frm_date);
        btn_simpan = (Button)findViewById(R.id.btn_simpan);

        spinnerStatus.setItems(daftarStatus);
        txt_welcome.setText("Welcom, " + student.name);

        selectedDate = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("dd MMM yyyy");
        txt_date.setText(df.format(selectedDate.getTime())) ;
    }

    private void setEvent(){
        frm_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment datepicker = new DatePickerFragment();
                datepicker.setTarget(new DatePickerFragment.OnCompleteListener() {
                    @Override
                    public void onComplete(Calendar calender, int target) {
                        DateFormat df = new SimpleDateFormat("dd MMM yyyy");
                        selectedDate = calender;
                        txt_date.setText(df.format(calender.getTime())) ;
                    }
                }, R.id.txt_date);


                int year = selectedDate.get(Calendar.YEAR);
                int month = selectedDate.get(Calendar.MONTH);
                int day = selectedDate.get(Calendar.DAY_OF_MONTH);
                datepicker.setDate(PilihanActivity.this, year, month, day);

                datepicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = daftarStatus[spinnerStatus.getSelectedIndex()];
                if(checkRadius || !status.toLowerCase().equals("hadir")){
                    Map<String, String> data = new HashMap<String, String>();
                    data.put("student_id", Integer.toString(student.id));
                    data.put("room_id",Integer.toString(listRoom.get(spinnerRoom.getSelectedIndex()).id));
                    data.put("date", Helper.getDate(selectedDate));
                    data.put("status", daftarStatus[spinnerStatus.getSelectedIndex()]);

                    final ProgressDialog progressDialog = new ProgressDialog(PilihanActivity.this);
                    progressDialog.setMessage("Loading ...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    ApiClient.getInstance(getBaseContext()).getApi().absent(data).enqueue(new Callback<ApiResponse>() {
                        @Override
                        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                            if (response.isSuccessful()){
                                if (response.body().status){
                                    Toast.makeText(getApplicationContext(), "Data berhasil di simpan", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getApplicationContext(), response.body().message, Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                            }

                            progressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<ApiResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(getBaseContext(), "Anda tidak berada pada lokasi sekolah" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getRooms() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiClient.getInstance(this).getApi().getRoom(student.id).enqueue(new Callback<RoomResponse>() {
            @Override
            public void onResponse(Call<RoomResponse> call, Response<RoomResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().status) {
                        if(response.body().data.size() != 0){
                            String[] kelas = new String[response.body().data.size()];
                            for (int i = 0; i < kelas.length; i++) {
                                kelas[i] = response.body().data.get(i).name;
                            }
                            spinnerRoom.setItems(kelas);
                            listRoom = response.body().data;
                            getLocation(progressDialog);
                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Anda tidak terdaftar dalam kelas", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RoomResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void getLocation(final ProgressDialog progressDialog){
        ApiClient.getInstance(this).getApi().getSchool().enqueue(new Callback<SchoolsResponse>() {
            @Override
            public void onResponse(Call<SchoolsResponse> call, Response<SchoolsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().status) {
                        school = response.body().data;
                        setSchoolLocation();
                    } else {
                        Toast.makeText(getApplicationContext(), response.body().message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<SchoolsResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.d("PERMITION", "Permition granted");
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.d("PERMITION", "Permition denied");
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void addLocationMarker(Location location){
        if (Gmap != null){
            try {
                if (location != null) {
                    LatLng position = new LatLng(location.getLatitude(),
                            location.getLongitude());
                    changeMap(position);

                    Gmap.clear();
                    Gmap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.getLatitude(), location.getLongitude())));
                    setSchoolLocation();
                }
            } catch (Exception e) {
                // TODO: handle exception
                Toast.makeText(getApplicationContext(), "Location not found", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    public void changeMap(LatLng location) {
        Gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16f));
    }

    public void setSchoolLocation(){
        if (Gmap != null){
            try {
                if (school != null){
                    LatLng position = new LatLng(school.latitude,
                            school.longitude);
                    changeMap(position);

                    MarkerOptions marker = new MarkerOptions()
                            .position(new LatLng(school.latitude, school.longitude));
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));
                    Gmap.addMarker(marker);

                    circle = Gmap.addCircle(new CircleOptions()
                            .center(new LatLng(school.latitude, school.longitude))
                            .radius(100)
                            .strokeColor(Color.RED));

                    float[] distance = new float[2];
                    Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                            circle.getCenter().latitude, circle.getCenter().longitude, distance);

                    if( distance[0] > circle.getRadius() ){
                        checkRadius = false;
                        Toast.makeText(getBaseContext(), "Anda tidak berada pada lokasi sekolah" , Toast.LENGTH_SHORT).show();
                    } else {
                        checkRadius = true;
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
                Toast.makeText(getApplicationContext(), "Lokasi sekolah tidak ditemukan", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}