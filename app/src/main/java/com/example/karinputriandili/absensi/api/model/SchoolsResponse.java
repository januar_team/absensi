package com.example.karinputriandili.absensi.api.model;

import com.example.karinputriandili.absensi.api.entity.SchoolEntity;

/**
 * Created by Januar-PC on 7/6/2017.
 */

public class SchoolsResponse extends ApiResponse {
    public SchoolEntity data;
}
