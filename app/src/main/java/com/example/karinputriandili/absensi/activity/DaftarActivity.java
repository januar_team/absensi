package com.example.karinputriandili.absensi.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.karinputriandili.absensi.Helper;
import com.example.karinputriandili.absensi.JSONParser;
import com.example.karinputriandili.absensi.R;
import com.example.karinputriandili.absensi.api.ApiClient;
import com.example.karinputriandili.absensi.api.entity.RoomEntity;
import com.example.karinputriandili.absensi.api.model.AbsentResponse;
import com.example.karinputriandili.absensi.api.model.RoomResponse;
import com.example.karinputriandili.absensi.ui.DatePickerFragment;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.jaredrummler.materialspinner.MaterialSpinner;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnDpWidthModel;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DaftarActivity extends AppCompatActivity implements DatePickerFragment.OnCompleteListener {
    protected Toolbar toolbar;
    private TableView<String[]> tableView;

    private FrameLayout date_from, date_to;
    private TextView txt_date_from, txt_date_to;
    private Calendar dateFrom, dateTo;
    private Button btnOk;

    private MaterialSpinner spinnerkelas;
    private List<RoomEntity> listRoom;

    private static final String[][] DATA_TO_SHOW = {{"This", "is", "a", "test"},
            {"and", "a", "second", "test"}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setFitsSystemWindows(true);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spinnerkelas = (MaterialSpinner) findViewById(R.id.spinnerkelas);

        initView();
        setEvent();
        getRooms();
        tableView = (TableView<String[]>) findViewById(R.id.tableView);
        TableColumnDpWidthModel columnModel = new TableColumnDpWidthModel(this, 9, 100);
        columnModel.setColumnWidth(0, 200);
        tableView.setColumnModel(columnModel);

    }

    private void initView() {
        date_from = (FrameLayout) findViewById(R.id.date_from);
        date_to = (FrameLayout) findViewById(R.id.date_to);
        txt_date_from = (TextView) findViewById(R.id.txt_date_from);
        txt_date_to = (TextView) findViewById(R.id.txt_date_to);

        Calendar c = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("dd MMM yyyy");
        dateFrom = c;
        txt_date_from.setText(df.format(c.getTime()));
        dateTo = c;
        txt_date_to.setText(df.format(c.getTime()));

        btnOk = (Button) findViewById(R.id.btn_ok);
    }

    private void setEvent() {
        date_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(R.id.date_from);
            }
        });

        date_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(R.id.date_to);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spinnerkelas.getSelectedIndex() > -1) {
                    Map<String, String> data = new HashMap<>();
                    data.put("room_id", Integer.toString(listRoom.get(spinnerkelas.getSelectedIndex()).id));
                    data.put("date_from", Helper.getDate(dateFrom));
                    data.put("date_to", Helper.getDate(dateTo));

                    final ProgressDialog progressDialog = new ProgressDialog(DaftarActivity.this);
                    progressDialog.setMessage("Loading ...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    ApiClient.getInstance(DaftarActivity.this).getApi().getAbsent(data).enqueue(new Callback<AbsentResponse>() {
                        @Override
                        public void onResponse(Call<AbsentResponse> call, Response<AbsentResponse> response) {
                            //JsonObject post = new JsonObject().get(response.body().toString()).getAsJsonObject();
                            if (response.isSuccessful()) {
                                if (response.body().status) {
                                    if(response.body().data.length > 0){
                                        int column = response.body().data[0].length;
                                        int row = response.body().data.length;
                                        tableView.setColumnCount(column);

                                        String[][] dataTable = new String[row][column];
                                        String[] header = new String[column];
                                        header[0] = "Nama";
                                        Calendar c = (Calendar) dateFrom.clone();
                                        for (int i = 1; i < column - 1; i++){
                                            header[i] = c.get(Calendar.DATE) + "/" + (c.get(Calendar.MONTH) + 1);
                                            c.add(Calendar.DATE, 1);
                                        }
                                        header[column - 1 ] = "Jumlah";

                                        for (int i = 0; i < row; i++){
                                            for (int j = 0; j < column; j++){
                                                dataTable[i][j] = response.body().data[i][j];
                                            }
                                        }

                                        tableView.setDataAdapter(new SimpleTableDataAdapter(DaftarActivity.this, dataTable));
                                        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(DaftarActivity.this, header));
                                    }

                                } else {
                                    Toast.makeText(DaftarActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(DaftarActivity.this, "Error connecting to server", Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<AbsentResponse> call, Throwable t) {
                            Toast.makeText(DaftarActivity.this, "Error connecting to server", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getRooms() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiClient.getInstance(this).getApi().getAllRoom().enqueue(new Callback<RoomResponse>() {
            @Override
            public void onResponse(Call<RoomResponse> call, Response<RoomResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().status) {
                        String[] kelas = new String[response.body().data.size()];
                        for (int i = 0; i < kelas.length; i++) {
                            kelas[i] = response.body().data.get(i).name;
                        }
                        spinnerkelas.setItems(kelas);
                        listRoom = response.body().data;
                    } else {
                        Toast.makeText(getApplicationContext(), response.body().message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<RoomResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void onBackPressed() {
        finish();
    }

    public void showDatePicker(int target) {
        DatePickerFragment datepicker = new DatePickerFragment();
        datepicker.setTarget(this, target);
        if (target == R.id.date_from) {
            if (dateFrom != null) {
                datepicker.setDate(this, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DATE));
            }
        } else if (target == R.id.date_to) {
            if (dateTo != null) {
                datepicker.setDate(this, dateTo.get(Calendar.YEAR), dateTo.get(Calendar.MONTH), dateTo.get(Calendar.DATE));
            } else {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                datepicker.setDate(this, year, month, day);
            }

            if (dateFrom != null) {
                datepicker.setMin(dateFrom.getTimeInMillis());
            }
        }
        datepicker.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onComplete(Calendar time, int target) {
        DateFormat df = new SimpleDateFormat("dd MMM yyyy");
        if (target == R.id.date_from) {
            dateFrom = time;
            txt_date_from.setText(df.format(time.getTime()));
        } else if (target == R.id.date_to) {
            dateTo = time;
            txt_date_to.setText(df.format(time.getTime()));
        }
    }
}





