package com.example.karinputriandili.absensi.ui;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.app.DatePickerDialog;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Januar on 7/1/2017.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private String mResult;
    private OnCompleteListener mListener;
    private int target;
    private DatePickerDialog pickerDialog;

    public static interface OnCompleteListener {
        public abstract void onComplete(Calendar calender, int target);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        if (pickerDialog == null) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            pickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        }
        // Create a new instance of DatePickerDialog and return it
        return pickerDialog;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year, monthOfYear, dayOfMonth);
        mListener.onComplete(c, target);
    }

    public void setDate(Activity activity, int year, int month, int day){
        pickerDialog = new DatePickerDialog(activity, this, year, month, day);
    }

    public void setTarget(OnCompleteListener listener, int target){
        this.mListener = listener;
        this.target = target;
    }

    public void setMax(Date max) {
        pickerDialog.getDatePicker().setMaxDate(max.getTime());
    }

    public void setMin(int min) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(date.getYear(),date.getMonth(),date.getDate() - min);
        pickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
    }

    public void setMin(Long timeInMillis) {
        pickerDialog.getDatePicker().setMinDate(timeInMillis);
    }
}
