package com.example.karinputriandili.absensi.api.model;

import com.example.karinputriandili.absensi.api.entity.RoomEntity;

import java.util.List;

/**
 * Created by Januar on 7/6/2017.
 */

public class RoomResponse extends ApiResponse {
    public List<RoomEntity> data;
}
