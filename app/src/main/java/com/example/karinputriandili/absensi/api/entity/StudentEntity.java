package com.example.karinputriandili.absensi.api.entity;

import java.io.Serializable;

/**
 * Created by Januar-PC on 7/6/2017.
 */

public class StudentEntity implements Serializable{
    public int id;
    public String username;
    public String name;
}
