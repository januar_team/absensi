package com.example.karinputriandili.absensi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;

import com.example.karinputriandili.absensi.R;
import com.example.karinputriandili.absensi.api.ApiClient;
import com.example.karinputriandili.absensi.api.model.LoginResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends ActionBarActivity {
    protected Toolbar toolbar;
    private EditText editTextUsername;
    private EditText editTextPassword;

    public static final String USER_NAME = "USERNAME";

    String username;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setFitsSystemWindows(true);
        setSupportActionBar(toolbar);

        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
    }

    public void back (View view){
        startActivity(new Intent(this, HomeActivity.class));
    }

    public void login (View view){
        username = editTextUsername.getText().toString();
        password = editTextPassword.getText().toString();
        login(username, password);
    }

    private void login(final String username, String password) {

        Map<String, String> data = new HashMap<String, String>();
        data.put("username", username);
        data.put("password", password);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Loading ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiClient.getInstance(this).getApi().login(data).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().status){
                        Intent intent = new Intent(LoginActivity.this, PilihanActivity.class);
                        intent.putExtra("user", response.body().data);
                        finish();
                        startActivity(intent);
                    }else{
                        Toast.makeText(LoginActivity.this, response.body().message, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(LoginActivity.this, "Error connecting to server", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Error connecting to server", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
