package com.example.karinputriandili.absensi.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.karinputriandili.absensi.R;

public class HelpActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    TextView judul, about, about2, about3, about4, about5, about6;
    String a1, a2, a3, a4, a5, a6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setFitsSystemWindows(true);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        judul = (TextView) findViewById(R.id.txtJudul);
        about = (TextView) findViewById(R.id.txtAbout);
        about2 = (TextView) findViewById(R.id.txtAbout2);
        about3 = (TextView) findViewById(R.id.txtAbout3);
        about4 = (TextView) findViewById(R.id.txtAbout4);
        about5 = (TextView) findViewById(R.id.txtAbout5);
        about6 = (TextView) findViewById(R.id.txtAbout6);

        a1 = "Cara penggunaan aplikasi ini sangat mudah. ";
        a2 = "Masukan Nomor Induk Siswa (NIS) jika siswa ingin mengisi kehadirannya.";
        a3 = "Jika siswa yang mengisi absen berada dalam jangkauan lingkungan sekolah.";
        a4 = "Maka otomatis akan terdaftar kehadiran siswa tersebut";
        a5 = "Sebaliknya, maka sistem absensi ini akan error.";
        a6 = "Kemudian para guru bisa melihat siswa-siswa yang hadir hari tersebut.";

        about.setText(String.valueOf(a1));
        about2.setText(String.valueOf(a2));
        about3.setText(String.valueOf(a3));
        about4.setText(String.valueOf(a4));
        about5.setText(String.valueOf(a5));
        about6.setText(String.valueOf(a6));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
