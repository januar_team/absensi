package com.example.karinputriandili.absensi.api;

import android.content.Context;

import com.example.karinputriandili.absensi.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Januar on 7/6/2017.
 */

public class ApiClient {
    private static ApiClient instance;
    private ApiInterface api;
    private Retrofit retrofit;
    private Context context;
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private ApiClient(Context context){
        this.context = context;
        api = getClient().create(ApiInterface.class);
    }

    public static String getURI(){
        return BuildConfig.API_URL;
    }

    public static String getApiUri(){
        return getURI() + "api/";
    }

    public Retrofit getClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        if (retrofit == null){
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(getApiUri())
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create());

            retrofit = builder.build();
        }
        return retrofit;
    }

    public ApiInterface getApi() {
        return api;
    }

    public static ApiClient getInstance(Context context){
        return (instance == null) ? new ApiClient(context) : instance;
    }
}
