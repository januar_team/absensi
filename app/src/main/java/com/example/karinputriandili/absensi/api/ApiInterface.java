package com.example.karinputriandili.absensi.api;

import com.example.karinputriandili.absensi.api.entity.StudentEntity;
import com.example.karinputriandili.absensi.api.model.AbsentResponse;
import com.example.karinputriandili.absensi.api.model.ApiResponse;
import com.example.karinputriandili.absensi.api.model.LoginResponse;
import com.example.karinputriandili.absensi.api.model.RoomResponse;
import com.example.karinputriandili.absensi.api.model.SchoolsResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Januar on 7/6/2017.
 */

public interface ApiInterface {

    @POST("room/absent")
    Call<AbsentResponse> getAbsent(@Body Map<String, String> data);

    @GET("room/getall")
    Call<RoomResponse> getAllRoom();

    @POST("student/login")
    Call<LoginResponse> login(@Body Map<String, String> data);

    @POST("student/room/{id}")
    Call<RoomResponse> getRoom(@Path("id") int id);

    @GET("school")
    Call<SchoolsResponse> getSchool();

    @POST("student/absent")
    Call<ApiResponse> absent(@Body Map<String, String> data);
}
