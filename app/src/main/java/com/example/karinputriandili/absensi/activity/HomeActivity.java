package com.example.karinputriandili.absensi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.karinputriandili.absensi.R;

public class HomeActivity extends AppCompatActivity {

    Button btnabout, btnlogin, btndaftar, btnhelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnabout = (Button) findViewById(R.id.btnabout);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        btndaftar = (Button) findViewById(R.id.btndaftar);
        btnhelp = (Button) findViewById(R.id.btnhelp);

        btnabout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent i = new Intent(HomeActivity.this, AboutActivity.class);
                startActivity(i);
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btndaftar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent i = new Intent(HomeActivity.this, DaftarActivity.class);
                startActivity(i);
            }
        });

        btnhelp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent i = new Intent(HomeActivity.this, HelpActivity.class);
                startActivity(i);
            }
        });


    }
}
