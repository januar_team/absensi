package com.example.karinputriandili.absensi.api.entity;

/**
 * Created by Januar-PC on 7/6/2017.
 */

public class SchoolEntity {
    public int id;
    public String name;
    public double longitude;
    public double latitude;
}
