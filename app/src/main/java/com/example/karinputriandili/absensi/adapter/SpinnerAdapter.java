package com.example.karinputriandili.absensi.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.example.karinputriandili.absensi.api.entity.RoomEntity;

/**
 * Created by Januar on 7/6/2017.
 */

public class SpinnerAdapter extends ArrayAdapter<RoomEntity> {

    public SpinnerAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }


}
