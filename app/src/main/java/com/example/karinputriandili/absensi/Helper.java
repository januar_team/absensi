package com.example.karinputriandili.absensi;

import java.util.Calendar;

/**
 * Created by Januar-PC on 7/6/2017.
 */

public class Helper {

    public static String getDate(Calendar calendar){
        return calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DATE);
    }
}
